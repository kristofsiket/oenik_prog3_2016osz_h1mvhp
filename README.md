Programozás III. beadandó előzetes tervezete
(Siket Kristóf)
 
A beadandó munkám egy klasszikus űrhajós játék lesz. Az űrhajót irányíthatjuk majd jobbra és balra (egyenesen magától fog haladni) az egér segítségével (az űrhajó „elveszi” majd a kurzort a rendszertől). Az űrben folyamatosan jönnek majd velünk szembe a meteorok, amelyeket ki kell kerülnünk, illetve szét kell lőnünk. A játék célja elsősorban az, hogy minél tovább életben maradjunk az űrben, illetve hogy minél több pontot szerezzünk. Pontokat kapunk folyamatosan, amíg életben vagyunk, de jelentősen megnövelhetjük a pontjaink számát annak függvényében, hogy hány darab meteort lövünk szét, ugyanis ilyenkor mindig újabb pontokat kapunk. Minél nagyobb egy meteor, annál több pontot ér, viszont annál többet kell bele lőnünk (így nagyobb a kockázata, hogy ütközzünk vele). Az eredményeket (név, pontszám, játékidő) minden menet után eltároljuk egy text fájlba. Ennek a tartalma megjeleníthető lesz a játék grafikus felületén („meghalás” után bekér egy nevet, mint a windowsos flipperben).  


![snapshot.jpg](https://bitbucket.org/repo/EK76bK/images/1135085849-snapshot.jpg)