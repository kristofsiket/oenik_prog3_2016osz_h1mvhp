using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// MVVM tervez�si mint�t haszn�lok, ez�rt sz�ks�g van erre a 'k�ztes r�tegre'.
    /// </summary>
    public class ViewModel : Bindable
    {
        private static ViewModel _peldany;

        private int pontok;
        private int eletek;
        private long ido;

        /// <summary>
        /// Az adatk�t�shez haszn�lt ViewModel.
        /// </summary>
        private ViewModel()
        {
            BtnColor = new SolidColorBrush(Color.FromArgb(160, 0, 176, 240));
            Eletek = 200;
            Meteorok = new ObservableCollection<Meteor>();
            Toltenyek = new ObservableCollection<Tolteny>();
            Hajo = new Urhajo(1350, 760); // be van dr�tozva...
            ScoresInit();
        }

        /// <summary>
        /// A men�gombok megjelen�t�s�hez haszn�lt ecsetet m�dos�tja �s adja vissza.
        /// </summary>
        public SolidColorBrush BtnColor { get; set; }

        /// <summary>
        /// A j�t�kos �leteinek sz�m�t m�dos�tja vagy adja vissza.
        /// </summary>
        public int Eletek
        {
            get
            {
                return eletek;
            }

            set
            {
                eletek = value;
                OPC();
            }
        }

        /// <summary>
        /// A Meteor objektumok m�g�tti gy�jtem�nyt m�dos�tja vagy adja vissza.
        /// </summary>
        public ObservableCollection<Meteor> Meteorok { get; set; }

        /// <summary>
        /// A T�lt�ny objektumok m�g�tt �ll� gy�jtem�nyt m�dos�tja vagy adja vissza.
        /// </summary>
        public ObservableCollection<Tolteny> Toltenyek { get; set; }

        /// <summary>
        /// A m�r lej�tszott j�t�kok adatait tartalmaz� list�t m�dos�tja vagy adja vissza. (forr�s: f�jl).
        /// </summary>
        public ObservableCollection<Jatek> Jatekok { get; set; }

        /// <summary>
        /// Az �rhaj�t m�dos�tja vagy adja vissza.
        /// </summary>
        public Urhajo Hajo { get; set; }

        /// <summary>
        /// A felhaszn�l� �ltal szerzett pontokat m�dos�tja vagy adja vissza.
        /// </summary>
        public int Pontok
        {
            get
            {
                return pontok;
            }

            set
            {
                pontok = value;
                OPC();
            }
        }

        /// <summary>
        /// A j�t�kban elt�lt�tt id�t milliszekundumban adja vissza.
        /// </summary>
        public long Ido
        {
            get
            {
                return ido;
            }

            set
            {
                ido = value;
                OPC();
            }
        }

        /// <summary>
        /// A ViewModel k�ls� el�r�s�t biztos�tja.
        /// </summary>
        /// <returns>Az egyetlen ViewModel objektumot a rendszerben.</returns>
        public static ViewModel Get()
        {
            if (_peldany == null)
            {
                _peldany = new ViewModel();
            }
            return _peldany;
        }

        /// <summary>
        /// A Jatekok list�t inicializ�lja egy f�jlbeolvas�ssal.
        /// </summary>
        public void ScoresInit()
        {
            Jatekok = new ObservableCollection<Jatek>();
            string[] sorok = File.ReadAllLines("../../eredmenyek.txt", Encoding.Default);

            for (int i = 0; i < sorok.Length; i++)
            {
                string[] sor = sorok[i].Split(' ');
                if (sor.Length == 3)
                {
                    Jatekok.Add(new Jatek() { Nev = sor[0], Pont = int.Parse(sor[1]), Ido = long.Parse(sor[2]) });
                }
            }
        }
    }
}