﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// Ez az osztály az űrhajót reprezentálja, amit majd az egérrel mozgatunk jobbra balra a játéktéren.
    /// </summary>
    public class Urhajo : JatekElem
    {
       /// <summary>
       /// A játékos űrhajója.
       /// </summary>
       /// <param name="aw">Ablak aktuális szélessége.</param>
       /// <param name="ah">Ablak aktuális magassága.</param>
        public Urhajo(double aw, double ah) : base()
        {
            Alak = new Rect(aw / 2, ah - 200, 100, 100);
            Console.WriteLine(ah + " " + aw);
        }

        /// <summary>
        /// Az űrhajó mozgása (csak x irányokba lehetséges!)
        /// </summary>
        /// <param name="dx">Oldalra elmozdulás mértéke</param>
        /// <param name="cw">Pálya szélének X koordinátája</param>
        public void Mozog(int dx, int cw)
        {
            if (Alak.Left + dx >= 0 && Alak.Right + dx <= cw)
            {
                Alak = new Rect(Alak.X + dx, Alak.Y, Alak.Width, Alak.Height);
            }
        }
    }
}
