﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// A játék üzleti logikáját tartalmazó osztály.
    /// </summary>
    public class BusinessLogic
    {
        private static BusinessLogic _peldany;
        private int szamlalo = 0; // ez azért kell, mert valamilyen oknál fogva többször hívódik a 'jatekvege' esemény (valószínűleg a timeres mozgás miatt)

        /// <summary>
        /// Játék végének bekövetkeztét jelző esemény.
        /// </summary>
        private EventHandler _jatekvege;

        /// <summary>
        /// A játék üzleti logikája.
        /// </summary>
        private BusinessLogic()
        {
            VM = ViewModel.Get();
            this.JatekMost = new Jatek();
        } 

        /// <summary>
        /// A játék végét jelző esemény.
        /// </summary>
        public event EventHandler JatekVege
        {
            add
            {
                if (_jatekvege == null)
                {
                    _jatekvege += value;
                }
            }

            remove
            {
                _jatekvege -= value;
            }
        }

        /// <summary>
        /// A ViewModel-t módosítja vagy adja vissza.
        /// </summary>
        public ViewModel VM { get; set; }

        /// <summary>
        /// Az aktuálisan játszott játék adatait tartalmazó objektum.
        /// </summary>
        public Jatek JatekMost { get; set; }

        /// <summary>
        /// A business-logic "getter" metódusa, amely létrehoz egy BusinessLogic példányt, ha még nem létezik.
        /// </summary>
        /// <returns>Az egyetlen BusinessLogic.</returns>
        public static BusinessLogic Get()
        {
            if (_peldany == null)
            {
                _peldany = new BusinessLogic();
            }
            return _peldany;
        }

        /// <summary>
        /// Egy text fájl egy sorába menti az aktuális játékot.
        /// </summary>
        public void JatekFajlbaMent()
        {
            string path = "../../eredmenyek.txt";
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            StreamWriter sw = File.AppendText(path);
            sw.WriteLine(JatekMost.Nev + " " + JatekMost.Pont + " " + JatekMost.Ido);
            sw.Close();
        }

        /// <summary>
        /// Egy meteor mozgatása (minden meteor objektum tick-re hívja önmagára).
        /// </summary>
        /// <param name="m">A mozgatott meteor.</param>
        /// <returns>Lenullázta-e a játékos életét.</returns>
        public bool MeteorMozog(Meteor m)
        {
            if (szamlalo == 0)
            {
                m.Mozog(); 
            }
            if (VM.Eletek == 0 && _jatekvege != null && szamlalo == 0)
            {
                m.DT.Stop();
                szamlalo++;
                _jatekvege(this, EventArgs.Empty);
                return true;
            }
            return false;
        }
    }
}
