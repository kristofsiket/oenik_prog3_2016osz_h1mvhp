﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// Az 'adatköthető' entitásokat reprezentáló absztrakt osztály.
    /// </summary>
    public abstract class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// A tulajdonságváltozást jelző esemény.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// PropertyChanged esemény "elsütése".
        /// </summary>
        /// <param name="n">A hívó property.</param>
        protected void OPC([CallerMemberName]string n = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(n));
            }
        }
    }
}
