﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// Az űrhajó által kilőtt töltényt reprezentálja.
    /// </summary>
    public class Tolteny : JatekElem
    {
        private int dy;

        /// <summary>
        /// Töltény osztály konstruktora. Létrehoz egy töltényt az azt kilövő űrhajó tetejében (tehát kizárólag kilövéskor hívódik meg!)
        /// </summary>
        /// <param name="kilovo">A töltényt kilövő űrhajó.</param>
        public Tolteny(Urhajo kilovo) : base()
        {
            Kilovo = kilovo;
            Alak = new Rect(Kilovo.Alak.X + 45, Kilovo.Alak.Y, 10, 10);
            Szin = new SolidColorBrush(Color.FromRgb(255, 80, 0));
            dy = 15;
            DT = new DispatcherTimer();
            DT.Interval = TimeSpan.FromMilliseconds(20);
            DT.Tick += DT_Tick;
            DT.Start();
        }

        /// <summary>
        /// A töltény megjelenítéséért felelős ecset.
        /// </summary>
        public Brush Szin { get; set; }

        /// <summary>
        /// A töltényt kilövő űrhajó.
        /// </summary>
        public Urhajo Kilovo { get; set; }

        /// <summary>
        /// A töltény kilövés utáni mozgásáért felelős timer.
        /// </summary>
        public DispatcherTimer DT
        {
            get; set;
        }

        /// <summary>
        /// A töltény mozgása az űrhajótól fölfelé.
        /// </summary>
        /// <param name="palyateteje">A pálya teteje.</param>
        /// <returns>Igaz, ha a töltény elérte a pálya tetejét.</returns>
        public bool Mozog(int palyateteje)
        {
            Alak = new Rect(Alak.X, Alak.Y - dy, Alak.Width, Alak.Height);
            Point p = new Point(Alak.X, Alak.Y);

            if (p.Y == palyateteje)
            {
                return true;
            }
            foreach (Meteor m in ViewModel.Get().Meteorok)
            {
                if (this.Alak.IntersectsWith(m.Alak))
                {
                    m.Eletek--;
                    this.Alak = new Rect();
                    ViewModel.Get().Toltenyek.Remove(this);
                }
            }
            return false;
        }

        private void DT_Tick(object sender, EventArgs e)
        {
            this.Mozog(0);
        }
    }
}
