﻿//-------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="OE-NIK">
//     No copyright (public domain)
// </copyright>
//-------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel VM;

        /// <summary>
        /// Az alkalmazás nyitóablaka.
        /// </summary>
        public MainWindow()
        {
            VM = ViewModel.Get();
            DataContext = VM;
            InitializeComponent();
        }

        private void lbl_Menu_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Label).Background.Opacity = 0.7;
        }

        private void lbl_Menu_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Label).Background.Opacity = 0.4;
        }

        private void scores_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ScoresWindow sw = new ScoresWindow();
            sw.ShowDialog();
        }

        private void lbl_Play_MouseDown(object sender, MouseButtonEventArgs e)
        {
            GameWindow gw = new GameWindow();
            gw.ShowDialog();
        }

        private void lbl_Quit_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }

    /// <summary>
    /// Mivel long típusú számban, milliszekundumban tároljuk az időket, ezért kell egy konverter.
    /// </summary>
    public class MillisecondsToTimeConverter : IValueConverter
    {      
        /// <summary>
        /// A milliszekundum-idő konverter algoritmusa.
        /// </summary>
        /// <param name="value">A bejövő számérték (long).</param>
        /// <param name="targetType">Cél típusa.</param>
        /// <param name="parameter">A paraméter</param>
        /// <param name="culture">Területi infó.</param>
        /// <returns>A megfelelő időformátumú string. (m:sec,msec)</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            long milli = (long)value;
            int mperc = (int)(milli / 1000);
            int msec = (int)milli - (mperc * 1000);
            int perc = mperc / 60;
            mperc = mperc % 60;
            string ki = perc + ":" + mperc + "," + msec;

            return ki;
        }

        /// <summary>
        /// A konverter convert-back része, most nem szükséges implementálnunk.
        /// </summary>
        /// <param name="value">A konvertálással kapott érték.</param>
        /// <param name="targetType">Cél típusa.</param>
        /// <param name="parameter">A paraméter</param>
        /// <param name="culture">Területi infó.</param>
        /// <returns>Most kivételt dob.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //-------------------INNENTŐL ESETLEGES VISUALOKRA ÁTÍRÁSA A PROGRAMNAK---------------------------//
}
