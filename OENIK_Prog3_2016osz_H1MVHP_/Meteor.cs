﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// A meteorokat reprezentáló osztály.
    /// </summary>
    public class Meteor : JatekElem
    {
        /// <summary>
        /// Statikus randomgenerátor.
        /// </summary>
        private static Random rnd = new Random();

        private int eletek;
        private int dy; // a meteor haladási tempója lesz, mivel csak 'y' irányba mozog, így dx nem kell
        private DispatcherTimer dt;

        /// <summary>
        /// Az űrhajóval szembe jövő meteor objektumok.
        /// </summary>
        public Meteor() : base()
        {
            Init();
            dy = 9;
            DT = new DispatcherTimer();
            DT.Interval = TimeSpan.FromMilliseconds(20);
            DT.Tick += DT_Tick;
            DT.Start();
        }

        /// <summary>
        /// A meteor folyamatos mozgásáért felelős timer.
        /// </summary>
        public DispatcherTimer DT
        {
            get
            {
                return dt;
            }

            set
            {
                dt = value;
                OPC();
            }
        }
        
        /// <summary>
        /// A meteor megjelenítéséért felelős ecsetet módosítja vagy adja vissza.
        /// </summary>
        public Brush Kep
        {
            get { return new ImageBrush(new BitmapImage(new Uri("meteor.jpg", UriKind.Relative))); }
        }

        /// <summary>
        /// Meteor életeinek számát módosítja vagy adja vissza.
        /// </summary>
        public int Eletek
        {
            get
            {
                return eletek;
            }

            set
            {
                eletek = value;
                OPC();
            }
        }

        /// <summary>
        /// A beérkező meteorok mozgásáért felelős eljárás.
        /// </summary>
        public void Mozog()
        {
            Alak = new Rect(Alak.X, Alak.Y + dy, Alak.Width, Alak.Height);
            if (this.Alak.IntersectsWith(ViewModel.Get().Hajo.Alak))
            {
                ViewModel.Get().Eletek--;
            }
            else
            {
                foreach (Tolteny t in ViewModel.Get().Toltenyek)
                {
                    if (this.Alak.IntersectsWith(t.Alak))
                    {
                        if (this.Eletek <= 0)
                        {
                            ViewModel.Get().Pontok += (int)((this.Alak.X * this.Alak.Y * 2) / 1000);
                            this.Alak = new Rect();
                            ViewModel.Get().Meteorok.Remove(this); 
                        }
                    }
                }
            }          
        }

        private void DT_Tick(object sender, EventArgs e)
        {
            if (BusinessLogic.Get().MeteorMozog(this))
            {
                dt.Stop();
            }
        }

        private void Init()
        {
            int meret = rnd.Next(70, 140); // ez biztosítja a megfelelő arányokat
            int pozX = rnd.Next(0, 1024);
            this.Alak = new Rect(pozX, 0, meret, meret);
            this.Eletek = meret / 50;
        }
    }
}
