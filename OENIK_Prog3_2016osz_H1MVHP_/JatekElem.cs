﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// Minden lehetséges játékelemre jellemző vonásokat tartalmazza.
    /// </summary>
    public abstract class JatekElem : Bindable
    {
        private Rect alak;

        /// <summary>
        /// Játékelem alakját módosítja vagy adja vissza.
        /// </summary>
        public Rect Alak
        {
            get
            {
                return alak;
            }

            set
            {
                alak = value;
                OPC();
            }
        }
    }
}
