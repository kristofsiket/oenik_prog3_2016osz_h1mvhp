﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// A játékmeneteket reprezentáló osztály.
    /// </summary>
    public class Jatek
    {
        /// <summary>
        /// Játékban elért időt módosítja vagy adja vissza.
        /// </summary>
        public long Ido { get; set; }

        /// <summary>
        /// Játékban elért pontot módosítja vagy adja vissza.
        /// </summary>
        public int Pont { get; set; }

        /// <summary>
        /// Játékos nevét módosítja vagy adja vissza
        /// </summary>
        public string Nev { get; set; }
    }
}
