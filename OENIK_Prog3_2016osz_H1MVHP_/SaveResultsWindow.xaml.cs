﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// A SaveResultWindow háttérlogikája.
    /// </summary>
    public partial class SaveResultsWindow : Window
    {
        private BusinessLogic BL;

        /// <summary>
        /// Játék végekor megjelenő, mentési adatokat bekérő ablak.
        /// </summary>
        public SaveResultsWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            this.DataContext = BL.VM;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtNev.Text == string.Empty)
            {
                MessageBox.Show("Kötelező nevet megadni!");
                e.Cancel = true;
            }
            else
            {
                BL.JatekMost = new Jatek() { Nev = txtNev.Text, Ido = BL.VM.Ido, Pont = BL.VM.Pontok };
                BL.JatekFajlbaMent();
                this.DialogResult = true;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            this.Close(); // a többi már megírva a closing esemény kezelőjébe
        }

        private void txtNev_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = e.Key == Key.Space;
        }
    }
    }
