﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace OENIK_Prog3_2016osz_H1MVHP_
{
    /// <summary>
    /// A GameWindow ablak codebehind-ja.
    /// </summary>
    public partial class GameWindow : Window
    {
        private ViewModel VM;
        private BusinessLogic BL;
        private Stopwatch sw;
        private DispatcherTimer dt;
        
        /// <summary>
        /// A játék-ablak.
        /// </summary>
        public GameWindow()
        {
            InitializeComponent();
            BL = BusinessLogic.Get();
            VM = BL.VM;
            DataContext = VM;
            BL.JatekVege += BL_JatekVege;
        }

        private void BL_JatekVege(object sender, EventArgs e)
        {
            sw.Stop();
            dt.Stop();
            VM.Ido = sw.ElapsedMilliseconds;
            this.Close();
            SaveResultsWindow srw = new SaveResultsWindow();
            srw.ShowDialog(); 
        }

        private void GameWindow_Loaded(object sender, RoutedEventArgs e)
        { 
            sw = new Stopwatch();
            sw.Start();
            this.DataContext = VM;
            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromSeconds(1.5);
            dt.Tick += Dt_Tick;
            dt.Start(); 
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            MeteorAdd();
        }

        private void cnv_MousMove_Handler(object sender, MouseEventArgs e)
        {
            if (e.GetPosition(sender as Canvas).X >= (sender as Canvas).ActualWidth || e.GetPosition(sender as Canvas).X < 0)
            {
                return;
            }
            VM.Hajo.Alak = new Rect(e.GetPosition(sender as Canvas).X, VM.Hajo.Alak.Y, VM.Hajo.Alak.Width, VM.Hajo.Alak.Height);
        }

        private void ToltenyAdd()
        {
            Ellipse e = new Ellipse();
            Tolteny t = new Tolteny(VM.Hajo);
            e.DataContext = t;
            e.SetBinding(Canvas.LeftProperty, new Binding("Alak.X"));
            e.SetBinding(Canvas.TopProperty, new Binding("Alak.Y"));
            e.SetBinding(Ellipse.WidthProperty, new Binding("Alak.Width"));
            e.SetBinding(Ellipse.HeightProperty, new Binding("Alak.Height"));
            e.SetBinding(Ellipse.FillProperty, new Binding("Szin"));
            VM.Toltenyek.Add(t);
            (this.Content as Canvas).Children.Add(e);
            if (VM.Toltenyek.Count > 10)
            {
                VM.Toltenyek.RemoveAt(0);
            }
        }

        private void MeteorAdd()
        {
            Ellipse e = new Ellipse();
            Meteor m = new Meteor();
            e.DataContext = m;
            e.SetBinding(Canvas.LeftProperty, new Binding("Alak.X"));
            e.SetBinding(Canvas.TopProperty, new Binding("Alak.Y"));
            e.SetBinding(Ellipse.WidthProperty, new Binding("Alak.Width"));
            e.SetBinding(Ellipse.HeightProperty, new Binding("Alak.Height"));
            e.SetBinding(Ellipse.FillProperty, new Binding("Kep"));
            VM.Meteorok.Add(m);
            (this.Content as Canvas).Children.Add(e);
            if (VM.Meteorok.Count > 5)
            {
                VM.Meteorok.RemoveAt(0);
            }
        }
   
        private void KilottToltenyHandler(object sender, EventArgs e)
        {
            if (VM.Toltenyek.Last().Mozog(0))
            {
                VM.Toltenyek.Remove(VM.Toltenyek.Last());
            }
        }

        private void mlb_Down_Handler(object sender, MouseButtonEventArgs e)
        {
            ToltenyAdd();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }
    }
}
